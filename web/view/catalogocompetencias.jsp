<%-- 
    Document   : catalogocompetencias
    Created on : 01-24-2020, 11:30:50 AM
    Author     : kevin.pazusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link href="https://fonts.googleapis.com/css?family=Bebas+Neue&display=swap" rel="stylesheet">
<link href="../resources/css/materialize.css" rel="stylesheet" type="text/css"/>
<link href="../resources/css/materialize.min.css" rel="stylesheet" type="text/css"/>
<script src="../resources/js/materialize.js" type="text/javascript"></script>
<script src="../resources/js/materialize.min.js" type="text/javascript"></script>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Catálogo Competencias</title>
    </head>
    <div class="container-fluid">
        <center>
            <label style="color: white ;font-size:60px;font-family: 'Bebas Neue', cursive;" > Catálogo de Competencias </label>
        </center>

        <div class="row">
            <table border="1" class="table table-striped" >
                <thead class="thead-dark">
                    <tr>
                        <th>Numero Competencias</th>
                        <th>Nombre</th>
                        <th>Descripcion</th>
                    </tr>
                </thead>
                <tbody >
                    <c:forEach items="${list}" var="compb">
                        <tr>
                            <td>${compb.id_competencias}</td>                                
                            <td>${compb.nombre}</td>
                            <td>${compb.descripcion}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
        ${msg}
    </div>
















</div>
</body>
</html>