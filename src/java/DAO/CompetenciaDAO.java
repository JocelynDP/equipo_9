package DAO;

import Model.CompetenciaBean;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;


public class CompetenciaDAO {

    Conexion con;

    public CompetenciaDAO(Conexion con) {
        this.con = con;
    }

    public boolean insertar(CompetenciaBean comp) {
        try {
            String sql = "insert into competencias values (?,?);";
            PreparedStatement ps = con.conectar().prepareStatement(sql);
            ps.setString(1, comp.getNombre());
            ps.setString(2, comp.getDescripcion());
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.out.println("Error en DAO Insertar");
            System.out.println("Error en" + e);
            return false;
        }
    }

    public boolean actualizar(CompetenciaBean comp) {
        try {
            String sql = "update competencias set nombre=?,descripcion=? where id_competencias=?";
            PreparedStatement ps = con.conectar().prepareStatement(sql);
            ps.setString(1, comp.getNombre());
            ps.setString(2, comp.getDescripcion());
            ps.setInt(9, comp.getId_competencias());
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.out.println("Error en DAO Actualizar");
            System.out.println("Error en" + e);
            return false;
        }
    }

    public boolean eliminar(int CompetenciaBean) {
        try {
            String sql = "delete from competencias where id_competencias=?";
            PreparedStatement ps = con.conectar().prepareStatement(sql);
            ps.setInt(1, CompetenciaBean);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.out.println("Error en DAO Eliminar");
            System.out.println("Error en" + e);
            return false;
        }
    }

    public List<CompetenciaBean> consultar() {
        try {
            String sql = "select * from competencias order by id_competencias";
            List<CompetenciaBean> lista = new LinkedList();
            PreparedStatement ps = con.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                CompetenciaBean comp = new CompetenciaBean(0);
                comp = new CompetenciaBean(rs.getInt(1));
                comp.setNombre(rs.getString(2));
                comp.setDescripcion(rs.getString(3));
                lista.add(comp);
            }
            return lista;
        } catch (SQLException e) {
            System.out.println("Error en DAO Consultar");
            System.out.println("Error en" + e);
            return null;
        }
    }
}

