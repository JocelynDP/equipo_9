package DAO;

import DAO.Conexion;
import Model.CompetenciaBean;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import Model.UsuariosBean;

public class UsuariosDAO {

    Conexion con;

    public UsuariosDAO(Conexion con) {
        this.con = con;
    }

    public boolean consultarUsuario(UsuariosBean user) {
        String sql = "select * from usuarios where usuario=? and pass=?";
        try {
            PreparedStatement ps = con.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            ps = con.conectar().prepareStatement(sql);
            ps.setString(1, user.getUsuario());
            ps.setString(2, user.getPass());
            rs = ps.executeQuery();
            if (rs.next()) {
                System.out.println("Entrando");
                return true;
            }
            return false;
        } catch (SQLException e) {
            System.out.println("Error login usuario");
            System.out.println("Error en: " + e.getMessage());
            return false;
        }
    }

    public boolean insertar(UsuariosBean usub) {
        try {
            String sql = "insert into usuarios values (?,?,?,?,?,?,?,?);";
            PreparedStatement ps = con.conectar().prepareStatement(sql);
            ps.setString(1, usub.getNombre());
            ps.setString(2, usub.getApellido());
            ps.setString(3, usub.getCorreo());
            ps.setString(4, usub.getDireccion());
            ps.setInt(5, usub.getEdad());
            ps.setString(6, usub.getUsuario());
            ps.setString(7, usub.getPass());
            ps.setInt(8, usub.getCompetencia().getId_competencias());
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.out.println("Error en DAO Insertar");
            System.out.println("Error en" + e);
            return false;
        }
    }

    public boolean actualizar(UsuariosBean usub) {
        try {
            String sql = "update usuarios set nombre=?,apellido=?,correo=?,direccion=?,edad=?,usuario=?,pass=?, id_competencias=? where id_usuarios=?";
            PreparedStatement ps = con.conectar().prepareStatement(sql);
            ps.setString(1, usub.getNombre());
            ps.setString(2, usub.getApellido());
            ps.setString(3, usub.getCorreo());
            ps.setString(4, usub.getDireccion());
            ps.setInt(5, usub.getEdad());
            ps.setString(6, usub.getUsuario());
            ps.setString(7, usub.getPass());
            ps.setInt(8, usub.getCompetencia().getId_competencias());
            ps.setInt(9, usub.getId_usuarios());
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.out.println("Error en DAO Actualizar");
            System.out.println("Error en" + e);
            return false;
        }
    }

    public boolean eliminar(int UsuarioBean) {
        try {
            String sql = "delete from usuarios where id_usuarios=?";
            PreparedStatement ps = con.conectar().prepareStatement(sql);
            ps.setInt(1, UsuarioBean);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.out.println("Error en DAO Eliminar");
            System.out.println("Error en" + e);
            return false;
        }
    }

    public List<UsuariosBean> consultar() {
        try {
            String sql = "select usu.id_usuarios, usu.nombre, usu.apellido, usu.correo, usu.direccion, usu.edad, usu.usuario, usu.pass, comp.nombre, comp.descripcion from usuarios usu\n"
                    + "inner join competencias comp on usu.id_competencias = comp.id_competencias order by id_usuarios ";
            List<UsuariosBean> lista = new LinkedList();
            PreparedStatement ps = con.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                UsuariosBean usub = new UsuariosBean(0);
                usub = new UsuariosBean(rs.getInt(1));
                usub.setNombre(rs.getString(2));
                usub.setApellido(rs.getString(3));
                usub.setCorreo(rs.getString(4));
                usub.setDireccion(rs.getString(5));
                usub.setEdad(rs.getInt(6));
                usub.setUsuario(rs.getString(7));
                usub.setPass(rs.getString(8));
                CompetenciaBean comp = new CompetenciaBean();
                comp.setNombre(rs.getString(9));
                comp.setDescripcion(rs.getString(10));
                usub.setCompetencia(comp);
                lista.add(usub);
            }
            return lista;
        } catch (SQLException e) {
            System.out.println("Error en DAO Consultar");
            System.out.println("Error en" + e);
            return null;
        }
    }
}
