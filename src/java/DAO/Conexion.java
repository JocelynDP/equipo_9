package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {

    Connection con = null;
    private String bd = "Equipo9";
    private String user = "kz";
    private String pass = "kzroot";
    private String url = "jdbc:mysql://usam-sql.sv.cds:3306/" + bd + "?useSSL=false";

    public Conexion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, user, pass);
            if (con != null) {
                System.out.println("Exito en conexion");
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Error de conexion");
            System.out.println("Error en: " + e);
        }
    }

    public Connection conectar() {
        return con;
    }

    public void desconectar() {
        try {
            con.close();
        } catch (SQLException e) {
            System.out.println("Error al desconectar");
            System.out.println("Error en: " + e);
        }
    }
}
    

