package Model;
public class UsuariosBean {
    private int id_usuarios;
    private String nombre;
    private String apellido;
    private String correo;
    private String direccion; 
    private int edad;
    private String usuario;
    private String pass;
    private CompetenciaBean competencia;

    public UsuariosBean() {
    }    
    
    public UsuariosBean(int id_usuarios) {
        this.id_usuarios = id_usuarios;
    }

    public int getId_usuarios() {
        return id_usuarios;
    }

    public void setId_usuarios(int id_usuarios) {
        this.id_usuarios = id_usuarios;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public CompetenciaBean getCompetencia() {
        return competencia;
    }

    public void setCompetencia(CompetenciaBean competencia) {
        this.competencia = competencia;
    }
    
    
}
