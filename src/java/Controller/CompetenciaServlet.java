package Controller;

import DAO.CompetenciaDAO;
import java.io.IOException;
import java.io.PrintWriter;
import DAO.Conexion;
import Model.CompetenciaBean;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import javax.servlet.RequestDispatcher;

public class CompetenciaServlet extends HttpServlet {

    Conexion con;
    CompetenciaDAO compDAO;
    CompetenciaBean compb;
    RequestDispatcher rd;
    String msg;
    boolean respuesta;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");

        switch (action) {
            case "insertar":
                insertar(request, response);
                break;
            case "leer":
                consultar(request, response);
                break;
            case "nuevo":
                nuevo(request, response);
                break;
            default:
                msg = "Error de action";
                request.setAttribute(msg, msg);
                consultar(request, response);
                break;
        }
    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String nombre = request.getParameter("nombre");
        String descripcion = request.getParameter("descripcion");
        CompetenciaBean compb = new CompetenciaBean(0);
        con = new Conexion();
        compb.setNombre(nombre);
        compb.setDescripcion(descripcion);
        compb.setId_competencias(0);
        compDAO = new CompetenciaDAO(con);
        respuesta = compDAO.insertar(compb);
        if (respuesta) {
            msg = "Datos Insertados";
        } else {
            msg = "Error al Insertar";
        }
        List<CompetenciaBean> list = compDAO.consultar();
        request.setAttribute("msg", msg);
        request.setAttribute("list", list);
        rd = request.getRequestDispatcher("/view/catalogocompetencias.jsp");
        rd.forward(request, response);
    }

    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        con = new Conexion();
        compb = new CompetenciaBean(0);
        compDAO = new CompetenciaDAO(con);
        List<CompetenciaBean> list = compDAO.consultar();
        request.setAttribute("list", list);
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/view/catalogocompetencia.jsp");
        rd.forward(request, response);
    }

    protected void nuevo(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        con = new Conexion();
        compb = new CompetenciaBean(0);
        compDAO = new CompetenciaDAO(con);
        CompetenciaDAO compDAO = new CompetenciaDAO(con);
        List<CompetenciaBean> list = compDAO.consultar();
        request.setAttribute("list", list);
        request.setAttribute("msg", msg);
        request.setAttribute("competenciaDAO", compDAO.consultar());
        rd = request.getRequestDispatcher("view/catalogocompetencias.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
